package uz.app.banking.core.webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/client")
public class ClientController {
    @GetMapping("/{page}")
    public String showPage(@PathVariable String page, Model model) {
        model.addAttribute("page", page);
        return "client/index";
    }
}
