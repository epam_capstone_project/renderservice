package uz.app.banking.core.webapp.controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import uz.app.banking.core.webapp.references.UserType;
import uz.app.banking.core.webapp.services.LoginService;

@Controller
public class LoginController {
    static Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    LoginService loginService;

    @GetMapping(value = "/login", produces = MediaType.TEXT_HTML_VALUE)
    public String loginPage() {
        return "login";
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.TEXT_HTML_VALUE)
    public String login(HttpServletRequest req, HttpServletResponse resp) {
        try {
            String token = loginService.getToken(req.getParameter("login"), req.getParameter("password"));
            UserType userType = loginService.getUserType(token);
            resp.addCookie(new Cookie("token", token));
            resp.setStatus(200);
            String redirectPath = "";
            switch (userType) {
                case CLIENT:
                    redirectPath = "/client/main";
                    break;
                case EMPLOYEE:
                    redirectPath = "/employee/main";
                    break;
                case ADMIN:
                    redirectPath = "/adm/main";
                    break;
                default:
                    redirectPath = "/login";
                    resp.setStatus(403);
            }
            resp.sendRedirect(redirectPath);
        } catch (Exception ex) {
            resp.setStatus(403);
            log.info("Error on logging in", ex);
        }
        return "login";
    }
}
