package uz.app.banking.core.webapp.references;

public enum UserType {
    CLIENT,
    EMPLOYEE,
    ADMIN
}
